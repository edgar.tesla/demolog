package manager;

import utils.ConnectionDB;
import utils.MapUtil;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Demo {
    private static final Logger LOGGER = Logger.getLogger(Demo.class.getName());
    private static final Map<String, String> DBPARAMS = MapUtil.MYMAP;

    public Demo() {
    }

    public static void LogMessage(boolean logToFile, boolean logToConsole, boolean logToDatabase,
                                  boolean logMessage, boolean logWarning, boolean logError,
                                  String messageText, boolean message, boolean warning, boolean error) throws Exception {
        messageText.trim();
        if (messageText == null || messageText.length() == 0) {
            return;
        }

        if (!logToConsole && !logToFile && !logToDatabase) {
            throw new Exception("Invalid configuration");
        }
        if ((!logError && !logMessage && !logWarning) || (!message && !warning && !error)) {
            throw new Exception("Error or Warning or Message must be specified");
        }

        toLogConsole(logToConsole, logMessage, logWarning, logError, messageText, message, warning, error);
        toLogFile(logToFile, logMessage, logWarning, logError, messageText, message, warning, error);
        toLogDatabase(logToDatabase, logMessage, logWarning, logError, messageText, message, warning, error);
    }

    private static void toLogDatabase(boolean logToDatabase, boolean logMessage,
                                      boolean logWarning, boolean logError,
                                      String messageText, boolean message,
                                      boolean warning, boolean error) throws SQLException {
        if (logToDatabase) {
            int t = 0;
            if (message && logMessage) {
                t = 1;
            }

            if (error && logError) {
                t = 2;
            }

            if (warning && logWarning) {
                t = 3;
            }
            Statement stmt = new ConnectionDB().getConnection().createStatement();
            stmt.executeUpdate("insert into Log_Values('" + message + "', " + String.valueOf(t) + ")");
        }
    }

    private static void toLogFile(boolean logToFile, boolean logMessage,
                                  boolean logWarning, boolean logError,
                                  String messageText, boolean message,
                                  boolean warning, boolean error) throws IOException {
        if (logToFile) {
            File logFile = new File(DBPARAMS.get("logFileFolder") + "/logFile.txt");
            if (!logFile.exists()) {
                logFile.createNewFile();
            }
            FileHandler fh = new FileHandler(DBPARAMS.get("logFileFolder") + "/logFile.txt");
            LOGGER.addHandler(fh);
            LOGGER.log(Level.INFO, messageText);
        }
    }

    private static void toLogConsole(boolean logToConsole, boolean logMessage,
                                     boolean logWarning, boolean logError,
                                     String messageText, boolean message,
                                     boolean warning, boolean error) {
        if (logToConsole) {
            ConsoleHandler ch = new ConsoleHandler();
            LOGGER.addHandler(ch);
            LOGGER.log(Level.INFO, messageText);
        }

    }
}
