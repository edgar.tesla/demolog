package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;

public class ConnectionDB {
    private static final Map<String, String> DBPARAMS = MapUtil.MYMAP;

    public Connection getConnection() throws SQLException {
        Connection connection = null;
        Properties connectionProps = new Properties();
        connectionProps.put("user", DBPARAMS.get("userName"));
        connectionProps.put("password", DBPARAMS.get("password"));

        connection = DriverManager.getConnection("jdbc:" + DBPARAMS.get("dbms") + "://" + DBPARAMS.get("serverName")
                + ":" + DBPARAMS.get("portNumber") + "/", connectionProps);
        return connection;
    }
}
