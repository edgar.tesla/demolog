package utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class MapUtil {
    public static final Map<String, String> MYMAP;

    static {
        Map<String, String> aMap = new HashMap<String, String>();
        aMap.put("userName", "postgres");
        aMap.put("password", "123456");
        aMap.put("dbms", "postgresql");
        aMap.put("serverName", "localhost");
        aMap.put("portNumber", "5432");
        aMap.put("logFileFolder", "");
        MYMAP = Collections.unmodifiableMap(aMap);
    }
}
