FROM openjdk:8-jdk-alpine
COPY "./target/demo-project-1.0-SNAPSHOT.jar" "demo-project.jar"
EXPOSE 6060
ENTRYPOINT ["java","-jar","demo-project.jar"]